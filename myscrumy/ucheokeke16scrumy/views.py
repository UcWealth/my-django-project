from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from . models import ScrumyGoals, GoalStatus, random_int_gen
from django.contrib.auth.models import User, Group
from . forms import SignUpForm, CreateGoalForm, MoveGoalForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages

def index(request):
    """ default view of ucheokeke16scrumy """
    form = SignUpForm()
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse('Your account has been created successfully')
    else:
        form = SignUpForm()
    my_group = Group.objects.get(name = 'Developer')
    all_users = User.objects.all()
    for i in all_users:
        my_group.user_set.add(i)
    context = {'form': form}
    return render(request, 'ucheokeke16scrumy/index.html', context)

@login_required
def move_goal(request, goal_id):
    template = 'ucheokeke16scrumy/movegoal.html'
    groups = Group.objects.all()
    dev = Group.objects.get(name = 'Developer').user_set.all()
    qa = Group.objects.get(name = 'Quality Assurance').user_set.all()
    admin = Group.objects.get(name = 'Admin').user_set.all()
    owner = Group.objects.get(name = 'Owner').user_set.all()
    top3 = [qa,admin,owner]
    message = messages.get_messages(request)

    weekly_goal = GoalStatus.objects.get(status_name="Weekly Goal")
    daily_goal = GoalStatus.objects.get(status_name="Daily Goal")
    verify_goal = GoalStatus.objects.get(status_name="Verify Goal")
    done_goal = GoalStatus.objects.get(status_name="Done Goal")

    obj = get_object_or_404(ScrumyGoals, goal_id=goal_id)
    form = MoveGoalForm(request.POST or None, instance=obj)
    if form.is_valid():
        current_user = request.user
        selected_value = request.POST.get('status_name')
        if current_user in admin or current_user in owner:
            obj.goal_status = GoalStatus.objects.get(status_name = request.POST.get('status_name'))
            obj.save()
            messages.success(request, "You successfully moved the goal")
            return redirect('/home/')
        elif current_user in qa:
            if current_user == obj.user:
                if obj.goal_status == done_goal:
                    if selected_value == 'Weekly Goal':
                        messages.error(request, "You cannot move back to Weekly Column")
                        return redirect('/home/')
                else:
                    obj.goal_status = GoalStatus.objects.get(status_name = request.POST.get('status_name'))
                    obj.save()
                    messages.success(request, "You successfully moved the goal")
                    return redirect('/home/')
            else:
                print("not my goal")
                if obj.goal_status == verify_goal:
                    if selected_value == 'Done Goal':
                        obj.goal_status = GoalStatus.objects.get(status_name = request.POST.get('status_name'))
                        obj.save()
                        messages.success(request, "You successfully moved the goal")
                        return redirect('/home/')
                else:
                    messages.error(request, "You can only move other users' verify goal to done goal")
                    return redirect('/home/')        
        elif current_user in dev:
            if obj.user == current_user:
                if selected_value == 'Done Goal':
                    messages.error(request, "You cannot move goals to Done Goal Column")
                    return redirect('/home/')
                else:
                    obj.goal_status = GoalStatus.objects.get(status_name = request.POST.get('status_name'))
                    obj.save()
                    messages.success(request, "You successfully moved the goal")
                    return redirect('/home/')
            else:
                print('You can only move your goals')
                messages.error(request, "You cannot move other users' goals!")
                return redirect('/home/')
        else:
            print('Kilode ni?')
                      
    context = {'dev':dev, 'qa':qa, 'admin':admin, 'owner': owner,'form':form, 'obj':obj, 'message':message}
    return render(request, template , context)
    

@login_required       
def add_goal(request):
    admin = Group.objects.get(name = 'Admin').user_set.all()
    weekly_goal = GoalStatus.objects.get(status_name="Weekly Goal")

    add_goal_form = CreateGoalForm()
    if request.method == 'POST':
        add_goal_form = CreateGoalForm(request.POST)
        if add_goal_form.is_valid():
            saveform = add_goal_form.save(commit=False)
            if saveform.user != request.user and request.user not in admin and saveform.goal_status != weekly_goal:
                return HttpResponse('You can only create weekly goals for yourself!')
            else:
                saveform.created_by = request.user
                saveform.save()
                print(saveform.goal_id)
                return HttpResponse("Goal Added Successfully!")
    else:
        add_goal_form = CreateGoalForm()
    context = {'add_goal_form': add_goal_form}
    return render(request, 'ucheokeke16scrumy/addgoal.html', context)

@login_required  
def home(request):
    users = User.objects.all()
    weekly_goal = GoalStatus.objects.get(status_name="Weekly Goal")
    daily_goal = GoalStatus.objects.get(status_name="Daily Goal")
    verify_goal = GoalStatus.objects.get(status_name="Verify Goal")
    done_goal = GoalStatus.objects.get(status_name="Done Goal")
    current_user = request.user
    storage = messages.get_messages(request)
    user_group = []
    for g in request.user.groups.all():
        user_group.append(g.name)

    context = {
        'users': users,
        'weekly_goal': weekly_goal,
        'daily_goal': daily_goal,
        'verify_goal': verify_goal,
        'done_goal': done_goal,
        'current_user': current_user,
        'message': storage, 
        'user_group': user_group
    }
    return render(request, 'ucheokeke16scrumy/home.html', context)
