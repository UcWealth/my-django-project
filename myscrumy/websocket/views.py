from django.shortcuts import render
from django.http import JsonResponse
from .models import Connection, ChatMessage
from django.views.decorators.csrf import csrf_exempt
import json
import boto3

@csrf_exempt
def test(response):
    return JsonResponse({'message': 'hello Daud'}, status=200)

def _parse_body(body):
    body_unicode = body.decode('utf-8') 
    return json.loads(body_unicode)

@csrf_exempt
def connect(request):
    if request.method == 'POST':
        print(request.body)
        body = _parse_body(request.body)
        connection_id = body['connectionId']
        # Save to database
        conn = Connection.objects.create(connection_id = connection_id)
        conn.save()
        return JsonResponse({'message': 'connected successfully'}, status=200)

@csrf_exempt
def disconnect(request):
    if request.method == 'POST':
        body = _parse_body(request.body)
        connection_id = body['connectionId']
        # Remove from database
        conn = Connection.objects.get(connection_id = connection_id)
        conn.delete()
        return JsonResponse({'message': 'disconnected successfully'}, status=200)

def _send_to_connection(connection_id, data):
    gatewayapi = boto3.client('apigatewaymanagementapi', endpoint_url='https://hdiajui7a9.execute-api.us-east-2.amazonaws.com/test/', region_name='us-east-2', aws_access_key_id='AKIAJMPXHUO6RROXDE3A', aws_secret_access_key='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')
    gatewayapi.post_to_connection(ConnectionId=connection_id, Data=json.dumps(data).encode('utf-8'))

@csrf_exempt
def send_message(request):
    body = _parse_body(request.body)
    ChatMessage.objects.create(username = body['body']["username"], message = body['body']["content"], timestamp = body['body']["timestamp"])
    connections = Connection.objects.all()
    data = {'messages': [body]}
    for conn in connections:
         _send_to_connection(conn.connection_id, data)
    return JsonResponse({'message': 'Message sent successfully!'}, status=200)

@csrf_exempt    
def get_recent_messages(request):
    messages = []
    get_messages = ChatMessage.objects.all()
    for i in get_messages:
        messages.append({"username": i.username, "content": i.message, "timestamp": i.timestamp})
    data = {"messages": messages}
    connections = Connection.objects.all()
    for conn in connections:
        _send_to_connection(conn.connection_id, data)
    return JsonResponse({'message':'All Recent Messages are served!'}, status=200)

