
import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
	README = readme.read()

# Allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


setup(name='django-ucheokeke16scrumy',
      version='1.0',
      packages=find_packages(),
      include_package_date=True,
      license='BSD License',
      description='A Django Enterprise chat app',
      author='Uchechukwu Okeke',
      author_email='ucheokeke16@gmail.com',
      url='https://www.example.com',
      classifiers=[
          'Development Status :: 4 - Beta',
          'Environment :: Web Environment',
          'Framework ::  Django',
          'Intended Audience :: End Users/Desktop',
          'Intended Audience :: Enterprises',
          'License :: OSI Approved :: BSD License',
          'Operating System :: OS Independent',
          'Operating System :: POSIX',
          'Programming Language :: Python',
          'Topic :: Communications :: Chat',
          'Topic :: Office/Business',
          'Topic :: Software Development :: Bug Tracking',
      ],	
)