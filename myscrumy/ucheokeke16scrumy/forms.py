from django.contrib.auth.models import User
from django.forms import ModelForm, Select, TextInput
from .models import ScrumyGoals, GoalStatus


class SignUpForm(ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username', 'password'] 

class CreateGoalForm(ModelForm):
    class Meta:
        model = ScrumyGoals
        fields = ['goal_name', 'user']
        widgets = {
            'goal_name' : TextInput(attrs={'class': 'form-control'}),
            'user' : Select(attrs={'class': 'form-control'}),
        }




        

class MoveGoalForm(ModelForm):
    class Meta:
        model = GoalStatus
        fields = ['status_name']
        widget = {
            'status_name': Select(attrs={'class': 'form-control'}),
        }
