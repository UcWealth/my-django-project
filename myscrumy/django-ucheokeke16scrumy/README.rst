
==================
Ucheokeke16scrumy
==================

Ucheokeke16scrumy is more than just a chat app. It is a great realtime organizational tool that can be used to foster better remote collaborations, project management and devops. It helps to define the future of work. 

Now that so many jobs are becoming increasingly remote, this tool steps in to bridge the gap.

To use this package, visit the link below to download for a free 14 day trial:

-> www.linuxjobber.com/products/ucheokeke16scrumy/downloads

Quick start
------------

1. Add 'ucheokeke16scrumy' to your INSTALLED_APPS settings like this::

	INSTALLED_APPS = [
		 ...
	    'ucheokeke16scrumy',
	]

2. Include the ucheokeke16scrumy URLconf in your project urls.py like this::
	path('ucheokeke16scrumy/', include('ucheokeke16scrumy.urls')),

3. Run `python manage.py migrate` to create the ucheokeke16scrumy models

4. Start the dev server and visit http://127.0.0.1:8000/ucheokeke16scrumy/ to make use of the app.

Happy Scruming! 

