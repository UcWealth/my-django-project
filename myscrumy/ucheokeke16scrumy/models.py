from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
import random
def random_int_gen():
    used_numbers = []
    for i in range(1000, 9999):
        random_int = random.randint(1000, 9999)
        if random_int not in used_numbers:
            used_numbers.append(random_int)
            return random_int
        else:
            continue


class GoalStatus(models.Model):
    CHOICES = [('Weekly Goal', 'Weekly Goal'), ('Daily Goal', 'Daily Goal'), ('Verify Goal', 'Verify Goal'), ('Done Goal', 'Done Goal')]
    status_name = models.CharField(max_length=100, choices=CHOICES)

    def __str__(self):
        return self.status_name


class ScrumyGoals(models.Model):
    objects = models.Manager()
    goal_id = models.IntegerField(default=random_int_gen)
    goal_name = models.CharField(max_length=100)
    created_by = models.CharField(max_length=100, blank=True)
    moved_by = models.CharField(max_length=100, blank=True)
    owner = models.CharField(max_length=100, blank=True)
    def weeklygoal():
        return GoalStatus.objects.get(id=1)
    goal_status = models.ForeignKey(GoalStatus, on_delete=models.PROTECT, default=weeklygoal)
    user = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name='scrumy_goals_user_set')

    def __str__(self):
        return self.goal_name


class ScrumyHistory(models.Model):
    moved_by = models.CharField(max_length=100)
    created_by = models.CharField(max_length=100)
    moved_from = models.CharField(max_length=100)
    moved_to = models.CharField(max_length=100)
    time_of_action = models.DateTimeField(default=timezone.now)
    goal = models.ForeignKey(ScrumyGoals, on_delete=models.PROTECT)

    def __str__(self):
        return self.created_by

# myuser = User.objects.get(username='louis')
# weeklygoal = GoalStatus.objects.get(status_name='Weekly Goal')
# ScrumyGoals.objects.create(goal_name='Learn Django', goal_id=1, created_by='Louis',
#  moved_by='Louis', owner='Louis', goal_status=weeklygoal, user=myuser)

#  creator = ScrumyGoals.objects.get(created_by='Louis')
#  creator.created_by = 'Oma'
#  creator.save()

#  dailygoal = GoalStatus.objects.get(status_name="Daily Goal")
#  weeklygoal = GoalStatus.objects.get(status_name="Weekly Goal")
#  donegoal = GoalStatus.objects.get(status_name="Done Goal")
#  goalstatus = ScrumyGoals.objects.get(goal_status=weeklygoal)
#  goalstatus.goal_status=dailygoal
#  goalstatus.save()
#  seconduser = User.objects.get(username='Wealth')
# ScrumyGoals.objects.create(goal_name='Master Software Development', goal_id=4, created_by='Wealth', moved_by='Wealth', owner='Wealth', goal_status=donegoal, user=seconduser)
